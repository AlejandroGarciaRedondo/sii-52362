//Autor: Alejandro García Redondo

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	mkfifo("/tmp/fifo",0777);
	int fd=open("/tmp/fifo", O_RDONLY);
	if(fd < 0){
		perror("Fallo en la apertura de la tuberia");
		return(1);
	}
	int aux;
	char buff[200];
	while(1)
	{
		aux=read(fd,buff,sizeof(buff));
		if(aux < 0){
			perror("Fallo en la lectura");
			return(1);
		}
		if(aux == 0){
			printf("Fin del juego\n");
			return(1);
		}
		printf("%s\n", buff);

	}
	close(fd);
	unlink("/tmp/fifo");
	return 0;
}
